package com.bren.model;

public class BusinessLogic implements DwellingModel {
    private DwellingService dwellingService;

    public BusinessLogic() {
        dwellingService = new DwellingService();
    }

    @Override
    public void printDwellingByPriceAndDistToKindergarten(
            int fromPrice, int toPrice, int fromDist, int toDist) {
        dwellingService.printDwellingByPriceAndDistToKindergarten(
                fromPrice, toPrice, fromDist, toDist);
    }

    @Override
    public void printDwellingByPrice(int from, int to) {
        dwellingService.printDwellingByPrice(from, to);
    }

    @Override
    public void printDwellingSquare(int from, int to) {
        dwellingService.printDwellingSquare(from, to);
    }

    @Override
    public void getInfo() {
        dwellingService.getInfo();
    }
}
