package com.bren.model;

public interface DwellingModel {
    void printDwellingByPriceAndDistToKindergarten(
            int fromPrice, int toPrice, int fromDist, int toDist);
    void printDwellingByPrice(int from, int to);
    void printDwellingSquare(int from, int to);
    void getInfo();
}
