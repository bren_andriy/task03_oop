package com.bren.model;
import com.bren.entity.Dwelling;
import com.bren.entity.Flat;
import com.bren.entity.IdBook;
import com.bren.entity.Penthouse;
import com.bren.entity.Mansion;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

public class DwellingService {
    private List<Dwelling> dwellings;


    DwellingService() {
        dwellings = new ArrayList<>();
        dwellings.add(new Penthouse(250,6,750000,300,
                1200,50,
                new IdBook(32542,2011),1,3));
        dwellings.add(new Penthouse(250,6,50000,1300,
                1200,50,
                new IdBook(25522,2000),1,3));
        dwellings.add(new Flat(89,2,250000,150,
                2000,1350,
                new IdBook(782112,2006),3));
        dwellings.add(new Mansion(390,12,1500000,1503,
                700,550,
                new IdBook(11247,2004),10,5));
        dwellings.add(new Mansion(230,8,1056000,1003,
                750,940,
                new IdBook(54311,2021),20,5));
        dwellings.add(new Flat(100,3,120000,900,
                1300,200,
                new IdBook(90212,1960),6));
        dwellings.add(new Flat(100,3,160000,650,
                1300,200,
                new IdBook(1268,1982),6));
    }

    private List<Dwelling> findByPriceAndDistToKindergarten
            (int fromPrice, int toPrice, int fromDist, int toDist) {
        return dwellings.stream()
                .filter(dwelling -> dwelling.getPrice() >= fromPrice)
                .filter(dwelling -> dwelling.getPrice() <= toPrice)
                .filter(dwelling -> dwelling.getDistanceToKindergarten() >= fromDist)
                .filter(dwelling -> dwelling.getDistanceToKindergarten() <= toDist)
                .collect(Collectors.toList());
    }

    void printDwellingByPriceAndDistToKindergarten
            (int fromPrice, int toPrice, int fromDist, int toDist) {
        List<Dwelling> byPriceAndDist = findByPriceAndDistToKindergarten(fromPrice,toPrice,fromDist,toDist);
        for (Dwelling dwelling:byPriceAndDist) {
            System.out.println(dwelling.getInfo());
        }
    }
    private List<Dwelling> findByPriceBetween(int from, int to) {
        return dwellings.stream()
                .filter(dwelling -> dwelling.getPrice() >= from)
                .filter(dwelling -> dwelling.getPrice() <= to)
                .collect(Collectors.toList());
    }

    void printDwellingByPrice(int from, int to) {
        List<Dwelling> byPriceBetween = findByPriceBetween(from,to);
        for (Dwelling dwelling:byPriceBetween) {
            System.out.println(dwelling.getInfo());
        }
    }

    private List<Dwelling> findBySquareBetween(int from, int to) {
        return dwellings.stream()
                .filter(dwelling -> dwelling.getSquare() >= from)
                .filter(dwelling -> dwelling.getSquare() <= to)
                .collect(Collectors.toList());
    }

    void printDwellingSquare(int from, int to) {
        List<Dwelling> bySquareBetween = findBySquareBetween(from,to);
        for (Dwelling dwelling:bySquareBetween) {
            System.out.println(dwelling.getInfo());
        }
    }

    public void getInfo() {
        ListIterator<Dwelling> iterator = dwellings.listIterator();
        while (iterator.hasNext()) {
            int i = iterator.nextIndex();
            System.out.println(i + ". " + iterator.next().getInfo());
        }
    }
}
