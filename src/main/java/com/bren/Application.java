package com.bren;

import com.bren.view.DwellingView;

public class Application {
    public static void main(String[] args) {
        new DwellingView().show();
    }
}
