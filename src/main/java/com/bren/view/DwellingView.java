package com.bren.view;

import com.bren.controller.DwellingController;
import com.bren.controller.DwellingControllerImpl;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class DwellingView {
    private DwellingController dwellingController;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public DwellingView() {
        dwellingController = new DwellingControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - print DwellingInfo");
        menu.put("2", "  2 - sortByPrice");
        menu.put("3", "  3 - sortBySquare");
        menu.put("4", "  4 - sortByPriceAndDistanceToKindergarten");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
    }

    private void pressButton1() {
        dwellingController.getInfo();
    }

    private void pressButton2() {
        System.out.println("Enter from: ");
        int from = Integer.parseInt(input.nextLine());
        System.out.println("Enter to: ");
        int to = Integer.parseInt(input.nextLine());
        dwellingController.printDwellingByPrice(from, to);
    }

    private void pressButton3() {
        System.out.println("Enter from: ");
        int from = Integer.parseInt(input.nextLine());
        System.out.println("Enter to: ");
        int to = Integer.parseInt(input.nextLine());
        dwellingController.printDwellingBySquare(from, to);
    }

    private void pressButton4() {
        System.out.println("Enter price from: ");
        int fromPrice = Integer.parseInt(input.nextLine());
        System.out.println("Enter price to: ");
        int toPrice = Integer.parseInt(input.nextLine());
        System.out.println("Enter distance from: ");
        int fromDist = Integer.parseInt(input.nextLine());
        System.out.println("Enter distance to: ");
        int toDist = Integer.parseInt(input.nextLine());
        dwellingController.printDwellingByPriceAndDistToKindergarten(
                fromPrice, toPrice, fromDist ,toDist);
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }


}
