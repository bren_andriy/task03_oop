package com.bren.entity;

public class Flat extends Dwelling {
    private int floor;

    public Flat(int square, int room, int price,
                int distanceToKindergarten, int distanceToSchool,
                int distanceToPlayground, IdBook idBook, int floor) {
        super(square, room, price, distanceToKindergarten,
                distanceToSchool, distanceToPlayground, idBook);
        this.floor = floor;
    }

    public String getInfo() {
        return "Flat: " + "Square: " + getSquare()
                + " km2, Number of room: " + getRoom()
                + ", Price: " + getPrice()
                + "$, distance to: Kindergarten - "
                + getDistanceToKindergarten()
                + "m, School - " + getDistanceToSchool()
                + "m, PlayGround - " + getDistanceToPlayground()
                + "m, Number of floor: " + floor
                + ",\n Id book: " + getIdBook().getNumber()
                + ", Data of delivery construction: "
                + getIdBook().getDateDelievery() + "\n";
    }
}
