package com.bren.entity;

public class Mansion extends Dwelling {
    private final int gazebo;
    private final int groundSquare;

    public Mansion(int square, int room, int price, int distanceToKindergarten,
                   int distanceToSchool, int distanceToPlayground,
                   IdBook idBook, int gazebo, int groundSquare) {
        super(square, room, price, distanceToKindergarten,
                distanceToSchool, distanceToPlayground, idBook);
        this.gazebo = gazebo;
        this.groundSquare = groundSquare;
    }

    public String getInfo() {
        return "Mansion: " + "Square: " + getSquare()
                + " km2, Number of room: " + getRoom()
                + ", Price: " + getPrice()
                + "$, distance to: Kindergarten - "
                + getDistanceToKindergarten()
                + "m, School - " + getDistanceToSchool()
                + "m, PlayGround - " + getDistanceToPlayground()
                + ",\n Id book: " + getIdBook().getNumber()
                + ", Data of delivery construction: "
                + getIdBook().getDateDelievery()
                + "m, gazebo: " + gazebo + ", groundSquare: " + groundSquare
                + " m2\n";
    }
}
