package com.bren.entity;

public class IdBook {
    private final int number;
    private final int dateDelievery;

    public IdBook(int number, int datePurchase) {
        this.number = number;
        this.dateDelievery = datePurchase;
    }

    int getNumber() {
        return number;
    }

    int getDateDelievery() {
        return dateDelievery;
    }
}
