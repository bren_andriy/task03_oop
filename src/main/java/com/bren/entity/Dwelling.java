package com.bren.entity;

public abstract class Dwelling {
    private final int square;
    private final int room;
    private final int price;
    private final int distanceToKindergarten;
    private final int distanceToSchool;
    private final int distanceToPlayground;
    private final IdBook idBook;


    public Dwelling(int square, int room, int price,
                    int distanceToKindergarten, int distanceToSchool,
                    int distanceToPlayground, IdBook idBook) {
        this.square = square;
        this.room = room;
        this.price = price;
        this.distanceToKindergarten = distanceToKindergarten;
        this.distanceToSchool = distanceToSchool;
        this.distanceToPlayground = distanceToPlayground;
        this.idBook = idBook;
    }

    public int getSquare() {
        return square;
    }

    int getRoom() {
        return room;
    }

    public int getPrice() {
        return price;
    }

    public int getDistanceToKindergarten() {
        return distanceToKindergarten;
    }

    int getDistanceToSchool() {
        return distanceToSchool;
    }

    int getDistanceToPlayground() {
        return distanceToPlayground;
    }

    public abstract String getInfo();

    public IdBook getIdBook() {
        return idBook;
    }
}
