package com.bren.entity;

public class Penthouse extends Dwelling {
    private final int pool;
    private final int terace;

    public Penthouse(int square, int room, int price, int distanceToKindergarten,
                     int distanceToSchool, int distanceToPlayground,
                     IdBook idBook, int pool, int terace) {
        super(square, room, price, distanceToKindergarten,
                distanceToSchool, distanceToPlayground, idBook);
        this.pool = pool;
        this.terace = terace;
    }

    public String getInfo() {
        return "Penthouse " + "Square: " + getSquare()
                + " km2, Number of room: " + getRoom()
                + ", Price: " + getPrice()
                + "$, distance to: Kindergarten - "
                + getDistanceToKindergarten()
                + "m, School - " + getDistanceToSchool()
                + "m, PlayGround - " + getDistanceToPlayground()
                + "m,\nId book: " + getIdBook().getNumber()
                + ", Data of delivery construction: "
                + getIdBook().getDateDelievery() + ", Teraces: "
                + terace + ", Pools: " + pool + "\n";
    }
}
