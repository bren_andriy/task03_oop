package com.bren.controller;

import com.bren.model.BusinessLogic;
import com.bren.model.DwellingModel;

public class DwellingControllerImpl implements DwellingController {
    private DwellingModel dwellingModel;

    public DwellingControllerImpl() {
        dwellingModel = new BusinessLogic();
    }

    @Override
    public void printDwellingByPriceAndDistToKindergarten(
            final int fromPrice, final int toPrice,
             final int fromDist, final int toDist) {
        dwellingModel.printDwellingByPriceAndDistToKindergarten(
                fromPrice, toPrice, fromDist, toDist);
    }

    @Override
    public void printDwellingBySquare(final int from,final int to) {
        dwellingModel.printDwellingSquare(from, to);
    }

    @Override
    public void printDwellingByPrice(final int from, final int to) {
        dwellingModel.printDwellingByPrice(from, to);
    }

    @Override
    public void getInfo() {
        dwellingModel.getInfo();
    }
}
