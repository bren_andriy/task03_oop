package com.bren.controller;

public interface DwellingController {
    void printDwellingByPriceAndDistToKindergarten(
            int fromPrice, int toPrice, int fromDist, int toDist);
    void printDwellingBySquare(int from, int to);
    void printDwellingByPrice(int from, int to);
    void getInfo();
}
